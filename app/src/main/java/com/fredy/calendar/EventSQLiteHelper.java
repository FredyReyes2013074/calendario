package com.fredy.calendar;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Fredux on 14/06/2015.
 */
public class EventSQLiteHelper extends SQLiteOpenHelper {

    private String sqlCreate="CREATE TABLE Event (id INTEGER PRIMARY KEY, title TEXT,description TEXT,date TEXT,time TEXT)";

    public EventSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIT Event");
        db.execSQL(sqlCreate);
    }
}
