package com.fredy.calendar;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


public class EventActivity extends ActionBarActivity {

    private TextView lblDate,txtTitle,txtDescription;
    private EditText txtHour,txtMin;
    private String time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);



        //Dates Loading
        txtHour=(EditText)findViewById(R.id.txtHour);
        txtMin=(EditText)findViewById(R.id.txtMins);
        txtTitle=(TextView)findViewById(R.id.txtTitulo);
        txtDescription=(TextView)findViewById(R.id.txtDescription);
        lblDate=(TextView)findViewById(R.id.lblDate);
        Bundle bundle = getIntent().getExtras();
        lblDate.setText(bundle.getString("date"));
        //End

    }

    public void eventClick(View view){
        time=txtHour.getText().toString()+":"+txtMin.getText().toString();

        if(txtTitle.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Title",Toast.LENGTH_LONG).show();
        }else{
            if(txtDescription.getText().toString().equals("")){
                Toast.makeText(getApplicationContext(),"Description",Toast.LENGTH_LONG).show();
            }else{
                if(txtHour.getText().toString().equals("") || txtMin.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Time",Toast.LENGTH_LONG).show();
                }else{
                    EventSQLiteHelper eventSQLiteHelper = new EventSQLiteHelper(this, "DBNeverMore", null, 1);
                    SQLiteDatabase db = eventSQLiteHelper.getWritableDatabase();

                    ContentValues newRegister = new ContentValues();
                    newRegister.put("title",txtTitle.getText().toString());
                    newRegister.put("description",txtDescription.getText().toString());
                    newRegister.put("date",lblDate.getText().toString());
                    newRegister.put("time",time);
                    db.insert("Event","",newRegister);

                    db.close();
                    Toast.makeText(getApplicationContext(),"Complete",Toast.LENGTH_LONG).show();
                    txtTitle.setText("");
                    txtDescription.setText("");
                    txtHour.setText("");
                    txtMin.setText("");
                    finish();
                }
            }
        }


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
